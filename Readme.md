# Assessment

Full case study assessment can be found [here](Task.md).

Implementation took approximately 36-40 working hours, including trying and rejecting first implementation approach and finalizing the second one. Also tests, javadoc and readme files also included into this amount.

# Description of the solution

### Technology

Test case is implemented as a _Spring Boot_ application, with REST API for job management.
Components and versions:

* Java 1.8
* Spring Boot 2.1.6
* Lombok 1.18.8

### Implementation approach description

Job management system is able to manage jobs if they are

* Inherited from `com.jobmgmt.job.AbstractJob`
* Annotated with `@com.jobmgmt.annotation.Job`
* Placed in package which is listed as `jobs.packages` setting in `application.yml` (or other file/environment please refer to Spring documentation`

Such classes can be added (as a jar, for example) to the classpath of pre-compiled job management application.

Please assume managing logging level and packages in `application.yml` as well

### Compilation prerequisites, compilation process and run

* Java 1.8
* Maven (tested on 3.3.9)
* Internet connection for downloading dependencies

Compilation is as simple as 

```bash
mvn clean install
```

Run the application using

```bash
java -jar target/job-management-system-1.0.0.jar
```

# Working with the application

### Logging

Please observe log messages in the console, default log settings forward all messages to the default output

### Interaction with the application

As far as it is application with REST API, please use your favorite tool for creating HTTP queries. I can advise [Postman](https://www.getpostman.com) as GUI tool and `curl` as command-line tool. All following examples will be provided with `curl` as the are more common

### Main application information

#### Health status

You can obtain very basic system health status by quering

```bash
curl -H "Accept: application/json"  -G http://localhost:8080/healthcheck
```

#### Jobs list

All registered jobs (complete jobs registry)

```bash
curl -H "Accept: application/json"  -G http://localhost:8080/jobs
```

All scheduled jobs (jobs with proper schedule for recurring run)

```bash
curl -H "Accept: application/json"  -G http://localhost:8080/jobs/scheduled
```

All unscheduled jobs (jobs which intended to run only once, in order of priority they have)

```bash
curl -H "Accept: application/json"  -G http://localhost:8080/jobs/unscheduled
```

**Please note.** Using one of the three URLs above, you can find out each job unique identifier - `uuid` field. To manage any specific job you need always specify this `uuid`

Fetch single job details:

```bash
curl -H "Accept: application/json"  -G http://localhost:8080/jobs/e149dddc-4768-4caa-903d-37cdc9178714
```

#### Job management

To reset the state of the job (i.e. - to run unscheduled job again), you can use

```bash
curl -H "Accept: application/json"  -G http://localhost:8080/jobs/e149dddc-4768-4caa-903d-37cdc9178714/reset
```

#### Job priority management

- To **fetch** current job priority, please run

```bash
curl -H "Accept: application/json"  -G http://localhost:8080/jobs/e149dddc-4768-4caa-903d-37cdc9178714/priority
```
Please note, that for scheduled (non prioritized) jobs priority is set to **`-1`**

- To **apply new** priority to the job, please run

```bash
curl -H 'Accept: application/json' \
     -H 'Content-Type: application/json' \
     -d '{"priority" : 100}' \
     http://localhost:8080/jobs/e149dddc-4768-4caa-903d-37cdc9178714/priority
```

Please note, that by applying new priority - any previous job schedule (if any) will be erased, any planned runs will be cancelled. However, it will not impact already running jobs

- To **delete** priority to the job, please run

```bash
curl -H 'Accept: application/json' \
     -X "DELETE"
     http://localhost:8080/jobs/e149dddc-4768-4caa-903d-37cdc9178714/priority
```

By deleting, priority will be set to **`-1`**.
This implementation does not control any priority boundries. So, negative priorities would also be accepted

#### Job schedule management

- To **fetch** current job schedule, please run

```bash
curl -H "Accept: application/json" -G http://localhost:8080/jobs/07e67841-8750-4083-ad81-47cfb7abbdcd/schedule
```
Please note, that for unscheduled (prioritized) jobs, this request will return **`Not scheduled`**

- To **apply new** schedule to the job, please run

```bash
curl -H 'Accept: application/json' \
     -H 'Content-Type: application/json' \
     -d '{"fixedDelay" : 3000}' \
     http://localhost:8080/jobs/e149dddc-4768-4caa-903d-37cdc9178714/schedule
```

Please note, that by applying new schedule - any previous job priority (if any) will be erased. However, it will not impact already running jobs

- To **delete** schedule for the job, please run

```bash
curl -H 'Accept: application/json' \
     -X "DELETE"
     http://localhost:8080/jobs/e149dddc-4768-4caa-903d-37cdc9178714/schedule
```

By deleting, priority will be set to **`-1`**.
This implementation does not control any priority boundries. So, negative priorities would also be accepted