# Coding assessment task - Job Management

## Purpose of the Case Study

The purpose of this Case Study is to prepare a draft technical design and prototypical code (compilable and at least partially covered by tests), based on a set of simple requirements. The design and code do not have to be fully complete or production ready, but it is required that the code can been tested to verify that it fulfills the desired requirements

## Case Study Details
A simple Job Management Service must be developed. The goal of this system is to handle the execution of multiple types of Jobs. The actions performed by these Jobs are not important; possible examples of these Jobs could be performing a data-load into a DWH, performing indexing of some file-based content or sending emails.

Features of this system are:

* **Flexibility**  
The types of possible actions performed by the Jobs are not known to the Job Management System. In the future, new Jobs should be supported without re-developing the Job Management System (optional).


* **Reliability**  
Each Job should either complete successfully or perform no action at all. (I.e. there should be no side-effects created by a Job that fails.)


* **Internal Consistency**  
At any one time a Job has one of four states: QUEUED, RUNNING, SUCCESS, FAILED. Following the execution of a Job, it should be left in an appropriate state.


* **Priority (Optional)**  
Each Job can be executed based on its priority relative to other Jobs


* **Scheduling**  
A Job can be executed immediately or according to a schedule.

**Case Study Tasks**
Prepare a prototype of such Job Management system. Provide one or two simple jobs to verify the fulfillment of requirements. Do not forget about configuration options :).
Where assumptions are made, these should be specified. If potential improvements or extensions are seen, these should be specified.