package com.jobmgmt.job;

import java.util.UUID;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jobmgmt.annotation.Job;
import com.jobmgmt.bean.Schedule;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Parent for every job which maybe processed and run by the application. Incorporates some of job management logic,
 * has extension points for implementations.
 * 
 * @author Sergii Sinelnychenko
 *
 */
@EqualsAndHashCode(of = "uuid")
public abstract class AbstractJob implements Runnable {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * Current job status
	 */
	private JobStatus status = JobStatus.QUEUED;
	
	@Getter
	@Setter
	private int priority;
	
	/**
	 * Unique job identifier. Generates during job instantiation, should be used to manage each job.
	 * Can be find out using {@link com.jobmgmt.controller.JobsRegistryController JobsRegistryController} endpoints
	 */
	@Getter
	private final String uuid = UUID.randomUUID().toString();
	
	@Getter
	private Schedule schedule;

	/**
	 * Reference to the {@link java.util.concurrent.ScheduledFuture ScheduledFuture} which (might) run this job.
	 * Used to cancel scheduled job
	 */
	@Getter
	private ScheduledFuture<?> future;
	
	/**
	 * Default constructor. Initializes job's priority and schedule out of {@link com.jobmgmt.annotation.Job @Job} annotation attributes
	 */
	public AbstractJob() {
		super();
		this.priority = this.getClass().getAnnotation(Job.class).priority();
		this.schedule = initSchedule();
		log.info("Job initialized");
		init();
	}

	/**
	 * Initialization callback for implementations
	 */
	public void doInit() {
	};

	/**
     * Reset callback for implementations
	 */
	public void doReset() {
	};

	/**
	 * Callback for real work load of the implementation
	 * @return <code>true</code> if job completed successfully, <code>false</code> otherwise
	 */
	abstract public boolean doRun();

	/**
	 * Initializes job
	 */
	public final void init() {
		synchronized (this.status) {
			if (this.status == JobStatus.QUEUED) {
				doInit();
			} else {
				log.error("Cannot initialize job with status {}", this.status);
			}
		}
	}

	/**
	 * Runs job. Checks statuses to be correctly set and managed
	 */
	public final void run() {
		synchronized (this.status) {
			if ((this.status == JobStatus.QUEUED) || (this.status == JobStatus.SUCCESS) ){
				this.status = JobStatus.RUNNING;
				try {
					if (doRun()) {
						this.status = JobStatus.SUCCESS;
					} else {
						this.status = JobStatus.FAILED;
					}
				} catch (Throwable t) {
					this.status = JobStatus.FAILED;
					log.error("Job run failed: " + t.getLocalizedMessage(), t);
				}
			} else {
				log.error("Cannot run job with status {}", this.status);
			}
		}
	}

	/**
	 * Resets job. Please note - that does not call {@link #init()} method
	 */
	public final void reset() {
		synchronized (this.status) {
			doReset();
			this.status = JobStatus.QUEUED;
		}
	}

	/**
	 * @return Current job status
	 */
	public final JobStatus getStatus() {
		return this.status;
	}
	
	/**
	 * @return Job name
	 */
	public String getName() {
		return this.getClass().getName();
	}

	/**
	 * @return Job schedule as human readable string
	 */
	public final String getScheduleString() {
		if (this.schedule == null) {
			return "Not scheduled";
		} else {
			return this.schedule.toString();
		}
	}
	
	public final boolean isScheduleValid() {
		return this.schedule.isScheduleValid();
	}
	
	/**
	 * Initializes schedule using {@link com.jobmgmt.annotation.Job @Job} annotation attributes
	 * @return {@link com.jobmgmt.bean.Schedule Schedule} instance, generated for this job
	 */
	private Schedule initSchedule() {
		Job jobAnnotation = this.getClass().getAnnotation(Job.class);
		Schedule schedule = Schedule.builder()
			.cron				(jobAnnotation.cron())
			.fixedDelay			(jobAnnotation.fixedDelay())
			.fixedRate			(jobAnnotation.fixedRate())
			.zone				(jobAnnotation.zone())
			.build();
		
			return schedule.isScheduleValid() ? schedule : null;
	}

	/**
	 * Applies new schedule
	 * 
	 * @param schedule New schedule to apply
	 */
	public final void applyNewSchedule(Schedule schedule) {
	    this.priority = -1;
		this.schedule = schedule;
	}

	public final void setFuture(ScheduledFuture<?> future) {
		this.future = future;
	}
}