package com.jobmgmt.job.sample;

import com.jobmgmt.annotation.Job;
import com.jobmgmt.job.AbstractJob;

/**
 * Job implementation sample. Used in demo purposes
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Job(priority = 5)
public class UnscheduledPrintJob05 extends AbstractJob {

    @Override
    public boolean doRun() {
        log.info(" 5 Another print job here!");
        return true;
    }

}
