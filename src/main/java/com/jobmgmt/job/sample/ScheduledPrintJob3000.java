package com.jobmgmt.job.sample;

import com.jobmgmt.annotation.Job;
import com.jobmgmt.job.AbstractJob;

/**
 * Job implementation sample. Used in demo purposes
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Job(fixedDelay = 3000)
public class ScheduledPrintJob3000 extends AbstractJob {

    @Override
    public boolean doRun() {
        log.info("3000 Print job here!");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.warn("Interrupted", e);
            return false;
        }
        return true;
    }

}
