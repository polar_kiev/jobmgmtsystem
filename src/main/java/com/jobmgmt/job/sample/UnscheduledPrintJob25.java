package com.jobmgmt.job.sample;

import com.jobmgmt.annotation.Job;
import com.jobmgmt.job.AbstractJob;

/**
 * Job implementation sample. Used in demo purposes
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Job(priority = 25)
public class UnscheduledPrintJob25 extends AbstractJob {

    @Override
    public boolean doRun() {
        log.info("25 Another print job here!");
        return true;
    }

}
