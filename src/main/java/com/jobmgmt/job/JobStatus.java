/**
 * 
 */
package com.jobmgmt.job;

/**
 * Enumeration of job statuses
 * 
 * @author Sergii Sinelnychenko
 *
 */
public enum JobStatus {
    /**
     * Job is properly initialized and ready for run
     */
	QUEUED, 
	
	/**
	 * Job is running
	 */
	RUNNING, 
	
	/**
	 * Job completed successfully, no threads running anymore.
	 * Available for reset
	 */
	SUCCESS,
	
	/**
	 * Job failed during run
	 */
	FAILED
}