package com.jobmgmt.component;

import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.jobmgmt.bean.Schedule;
import com.jobmgmt.event.RescheduleJobsEvent;
import com.jobmgmt.job.AbstractJob;
import com.jobmgmt.job.JobStatus;

/**
 * Main logic for job management. Uses {@link com.jobmgmt.component.JobsRegistry JobsRegistry} as a job data storage
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Service
public class JobManagementService {

	private Logger log = LoggerFactory.getLogger(JobManagementService.class);

	@Autowired
	private JobsRegistry jobsRegistry;

	@Autowired
	@Qualifier("taskExecutor")
	private TaskExecutor taskExecutor;

	@Autowired
	private ThreadPoolTaskScheduler scheduler;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * Captures {@link org.springframework.boot.context.event.ApplicationReadyEvent ApplicationReadyEvent} 
     * to run all jobs related propcesses only when whole application is up and running
     */
    @EventListener(ApplicationReadyEvent.class)
	void onApplicationReadyEvent() {
		runUnscheduledJobsByPriority();
		scheduleJobs();
	}

    /**
     * Captures custom {@link com.jobmgmt.event.RescheduleJobsEvent RescheduleJobsEvent} to check again
     * which jobs should be run according to their priorities.
     * Triggered when any job schedule/priority changed.
     * Subject for extended use
     */
	@Async
	@EventListener(RescheduleJobsEvent.class)
	void onRescheduleJobsEvent() {
		runUnscheduledJobsByPriority();
	}

	/**
	* Iterates through {@link com.jobmgmt.component.JobsRegistry JobsRegistry}'s scheduled job to activate them
	*/
	private void scheduleJobs() {
		log.debug("Running scheduled jobs");
		jobsRegistry.getScheduledRegistry().stream()
			.filter(j -> j.getStatus().equals(JobStatus.QUEUED))
			.forEach(this::scheduleTaskForJob);
	}

	/**
	 * Schedules job for execution using {@link org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler ThreadPoolTaskScheduler}
	 * @param job Job to schedule
	 */
	private void scheduleTaskForJob(AbstractJob job) {
		if (job.isScheduleValid()) {
			ScheduledFuture<?> future = null;
			Schedule jobSchedule = job.getSchedule();

			if (jobSchedule.getFixedDelay() > -1) {
			
				future = scheduler.scheduleWithFixedDelay(job, jobSchedule.getFixedDelay());

			} else if (jobSchedule.getFixedRate() > -1) {
			
				future = scheduler.scheduleAtFixedRate(job, jobSchedule.getFixedDelay());
			
			} else if (StringUtils.hasText(jobSchedule.getCron())) {
			
				String zone = jobSchedule.getZone();
				TimeZone timeZone;
				if (StringUtils.hasText(zone)) {
					timeZone = StringUtils.parseTimeZoneString(zone);
				} else {
					timeZone = TimeZone.getDefault();
				}
				scheduler.schedule(job, new CronTrigger(jobSchedule.getCron(), timeZone));
				
			}
			
			job.setFuture(future);
		} else {
			log.warn("Job {} with uuid {} was not scheduled due to incorrect schedule: {}", job, job.getUuid(), job.getScheduleString());
		}
	}

	/**
	 * Iterates through {@link com.jobmgmt.component.JobsRegistry JobsRegistry}'s unscheduled (prioritized) jobs to run them.
	 * Such jobs are already sorted in the registry according to priorities.
	 */
	private void runUnscheduledJobsByPriority() {
		log.debug("Running unscheduled jobs");
		jobsRegistry.getUnscheduledRegistry()
			.stream()
			.filter(j -> j.getStatus().equals(JobStatus.QUEUED))
			.forEach(scheduler::execute);
	}

	/**
	 * Removes schedule information from the job, removes any scheduled task for it
	 * 
	 * @param uuid Identifier of the job
	 * @return <code>true</code> if job was found and schedule was successfully reset, <code>false</code> otherwise
	 */
	public boolean unscheduleJob(String uuid) {
		return unscheduleJob(this.jobsRegistry.findJobByUuid(uuid));
	}

	/**
	 * Resets job to initial state, allowing it to be processed again
	 * @param job Job to reset
	 */
	public void resetJob(AbstractJob job) {
		job.reset();
		applicationEventPublisher.publishEvent(new RescheduleJobsEvent(this));
	}

	/**
	 * Applies new schedule to the specified job
	 * @param jobuuid Identifier of the job
	 * @param schedule Schedule to apply
	 * 
	 * @return Error message if there was an error, <code>null</code> if no errors occurred
	 */
	public String applyNewSchedule(String jobuuid, Schedule schedule) {
		boolean isOriginallyScheduled = true;
		AbstractJob job = jobsRegistry.findScheduledJob(jobuuid);
		if (job == null) {
			isOriginallyScheduled = false;
			job = jobsRegistry.findUnscheduledJob(jobuuid);
		}
		if (job == null) {
			return "Job not found";
		}
		synchronized (job) {
			if (isOriginallyScheduled) {	//	There should be ScheduledTask for it
				unscheduleJob(job);
			}
			job.applyNewSchedule(schedule);
			if (!isOriginallyScheduled) {
				jobsRegistry.moveToScheduled(job);
			}
			scheduleTaskForJob(job);
		}
		return null;
	}

    /**
     * Removes schedule information from the job, removes any scheduled task for it
     * 
     * @param job Job to unschedule
     * @return <code>true</code> if job was found and schedule was successfully reset, <code>false</code> otherwise
     */
	private boolean unscheduleJob(AbstractJob job) {
	    if (job == null) {
	        return false;
	    }
		ScheduledFuture<?> future = job.getFuture();
		if (future != null) {
			job.applyNewSchedule(null);
			job.setFuture(null);
			return future.cancel(false);
		}
		return false;
	}

	/**
     * Applies new priority to the specified job
     * @param jobuuid Identifier of the job
     * @param priority Priority to apply
     * 
     * @return Error message if there was an error, <code>null</code> if no errors occurred
	 */
	public String applyNewPriority(String jobuuid, int priority) {
		AbstractJob job = this.jobsRegistry.findScheduledJob(jobuuid);
		if (job != null) {
		    unscheduleJob(job);
		    jobsRegistry.moveToUnscheduled(job);
		} else {
		    job = this.jobsRegistry.findUnscheduledJob(jobuuid);
	        if (job == null) {
	            return "Job not found";
	        }
		}
		job.setPriority(priority);
		applicationEventPublisher.publishEvent(new RescheduleJobsEvent(this));
		return null;
	}
}
