package com.jobmgmt.component;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import com.jobmgmt.annotation.Job;
import com.jobmgmt.configuration.JobsManagementProperties;
import com.jobmgmt.job.AbstractJob;

import lombok.Getter;

/**
 * Jobs registry. Keeps track of job records, manages both scheduled (i.e. run by specific schedule - recurring jobs)
 * and not scheduled - typically jobs run once
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Component
public class JobsRegistry {

	private Logger log = LoggerFactory.getLogger(JobsRegistry.class);

	/**
	 * Registry for unscheduled jobs. Sorted by priority
	 */
	@Getter
	private Set<AbstractJob> unscheduledRegistry = new TreeSet<AbstractJob>(Comparator.comparing(AbstractJob::getPriority));

	/**
	 * Scheduled jobs registry. Sorted by UUID
	 */
	@Getter
	private Set<AbstractJob> scheduledRegistry = new TreeSet<AbstractJob>(Comparator.comparing(AbstractJob::getUuid));

	@Autowired
	private JobsManagementProperties jobsManagementProperties;

	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * Initializes jobs registry after this Spring bean instantiation.
	 * Scans packages defined in settings for classes matching required conditions - 
	 * with proper parent ({@link com.jobmgmt.job.AbstractJob AbstractJob} and annotated
	 * with proper annotation ({@link com.jobmgmt.annotation.Job @Job}).
	 * Any class annotated with it is Spring Bean, and scan process makes sure these beans 
	 * are properly loaded into the context.
	 * Also, initial schedule and priorities are recognized and properly recorded
	 */
	@PostConstruct
	private void init() {
		log.debug("Job registry initialization started");
		
		scan();
		
		if (log.isDebugEnabled()) {
			unscheduledRegistry.stream().forEach(e -> {
				log.debug("Registered unscheduled job {} with order {}", e.toString(), e.getPriority());
			});
			scheduledRegistry.stream().forEach(e -> {
				log.debug("Registered scheduled job {} (priority is ignored)", e);
			});
			log.debug("Job registry initialized");
		}
	}

	/**
     * Scans packages defined in settings for classes matching required conditions - 
     * with proper parent ({@link com.jobmgmt.job.AbstractJob AbstractJob} and annotated
     * with proper annotation ({@link com.jobmgmt.annotation.Job @Job}.
     * Any class annotated with it is Spring Bean, and scan process makes sure these beans 
     * are properly loaded into the context.
     * Also, initial schedule and priorities are recognized and properly recorded
	 */
	@SuppressWarnings("unchecked")
    void scan() {
	    ClassPathScanningCandidateComponentProvider componentProvider = configureClasspathScanningComponentProvider();
		final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		String[] packagesToScan = jobsManagementProperties.getPackages();
		if (packagesToScan == null) {
		    packagesToScan = new String[] {};
		}
        Arrays.stream(packagesToScan)
				.map(componentProvider::findCandidateComponents)
				.flatMap(m -> m.stream())
				.collect(Collectors.toSet())
				.forEach(c -> {
					log.debug("Found annotated class : " + c.getBeanClassName());
					try {
						Class<?> clazzUnknown = ClassUtils.forName(c.getBeanClassName(), contextClassLoader);
						if (AbstractJob.class.isAssignableFrom(clazzUnknown)) {
							registerJobInstance((Class <? extends AbstractJob>) clazzUnknown);
						} else {
							log.warn("Found class {} annotated as @Job, but not an ansector of {}", clazzUnknown.getName(), Job.class.getName());
						}
					} catch (ClassNotFoundException | LinkageError | SecurityException | IllegalArgumentException e) {
						log.error("Error initializing job instance: " + c.getBeanClassName(), e);
					}
				});
	}

    /**
     * Registers job by its class. Makes sure the bean is instantiated and loaded into the context.
     * Also, according to {@link com.jobmgmt.annotation.Job @Job} attributes, adds information to proper registry part
     * 
     * @param clazz Class name to process
     * 
     * @see #scheduledRegistry
     * @see #unscheduledRegistry
     */
	@SuppressWarnings("unchecked")
    protected <T> void registerJobInstance(Class<T> clazz) {
	    String beanName = clazz.getCanonicalName();
        Map<String, AbstractJob> beans = (Map<String, AbstractJob>) applicationContext.getBeansOfType(clazz);
        // According to specification cannot return null
        if (beans.size() < 1) {
            if (applicationContext instanceof GenericApplicationContext) {
                ((GenericApplicationContext)applicationContext).registerBean(
                    beanName,
                    clazz, 
                    () -> {
                        try {
                            return clazz.newInstance();
                        } catch (InstantiationException | IllegalAccessException e) {
                            log.error("Error registering bean " + beanName, e);
                        }
                        return null;
                    }
                );
                
                beans.put(beanName, (AbstractJob) applicationContext.getBean(beanName));
            } else {
                log.warn("Cannot register job {} because it is not initialized by Spring as bean, and current application " + 
                         "context {} is not an instance of org.springframework.context.support.GenericApplicationContext", 
                         clazz.getName(), applicationContext.getClass().getName());
            }
        }
        Job classAnnotation = clazz.getAnnotation(Job.class);
        log.debug("Job annotation values: priority {}, fixed delay {}, fixed rate {}, zone {}, cron {}",
        		classAnnotation.priority(), classAnnotation.fixedDelay(), classAnnotation.fixedRate(),
        		classAnnotation.zone(), classAnnotation.cron());
        Function<? super AbstractJob, Boolean> addAction = hasAnySchedule(classAnnotation) ? scheduledRegistry::add
        																				   : unscheduledRegistry::add;
        beans.values().stream().forEach(v -> {
        										if (!addAction.apply(v)) {
        											log.warn("Not added: {}", v);
        										};
        });
    }

    /**
     * Configures Spring's {@link org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider ClassPathScanningCandidateComponentProvider}
     * to search for classes annotated with {@link com.jobmgmt.annotation.Job @Job}
     */
    protected ClassPathScanningCandidateComponentProvider configureClasspathScanningComponentProvider() {
        final ClassPathScanningCandidateComponentProvider componentProvider = new ClassPathScanningCandidateComponentProvider(false);
		componentProvider.addIncludeFilter(new AnnotationTypeFilter(Job.class));
		return componentProvider;
    }

	/**
	 * Checks provided annotation instance for any schedule defined
	 * 
	 * @param classAnnotation Annotation instance to analyze
	 * @return <code>true</code> if any schedule attributes were provided with the annotation, <code>false</code> otherwise. 
	 * In this case, it is anticipated priority to be set. If none was set - default priority will be used
	 * 
	 * @see {@link  com.jobmgmt.annotation.Job#priority() @Job#priority}
	 */
	private boolean hasAnySchedule(Job classAnnotation) {
		return (classAnnotation.fixedDelay() >= 0) 
				|| (classAnnotation.fixedRate() >= 0)
				|| (StringUtils.hasText(classAnnotation.cron())
			   );
	}

	/**
	 * Finds job by UUID in both registries
	 * 
	 * @param jobId UUID of job to find
	 * @return Found job instance or <code>null</code> if no job was found
	 */
	public AbstractJob findJobByUuid(String jobId) {
		AbstractJob j = findUnscheduledJob(jobId);
		return j != null ? j : findScheduledJob(jobId);
	}

    /**
     * Finds job by UUID in scheduled jobs registry
     * 
     * @param jobId UUID of job to find
     * @return Found job instance or <code>null</code> if no job was found
     * @see #scheduledRegistry
     */
	public AbstractJob findScheduledJob(String jobId) {
		return doSearchById(scheduledRegistry, jobId);
	}

    /**
     * Finds job by UUID in unscheduled jobs registry
     * 
     * @param jobId UUID of job to find
     * @return Found job instance or <code>null</code> if no job was found
     * @see #unscheduledRegistry
     */
	public AbstractJob findUnscheduledJob(String jobId) {
		return doSearchById(unscheduledRegistry, jobId);
	}

	/**
	 * Performs search in provided collection (registry) for job with UUID specified
	 * @param registry Registry to look through
     * @param jobId UUID of job to find
     * @return Found job instance or <code>null</code> if no job was found
	 */
	private AbstractJob doSearchById(Collection<AbstractJob> registry, String jobId) {
		return registry.stream().filter(j -> j.getUuid().equals(jobId)).findFirst().orElse(null);
	}

	/**
	 * Merges both registries and returns as complete set. Sometimes required
	 * @return Both registries merged
     * @see #scheduledRegistry
     * @see #unscheduledRegistry
	 */
	public Set<AbstractJob> getCompleteRegistry() {
		return Stream.of(this.scheduledRegistry, this.unscheduledRegistry)
				.flatMap(Set::stream)
				.collect(Collectors.toSet());
	}

	/**
	 * Moves job from unscheduled to scheduled jobs registry.
	 * Performs no verification if the job specified has proper schedule set,
	 * does not adjust priority
	 * 
	 * @param job Job to move to scheduled jobs registry 
	 */
	public void moveToScheduled(AbstractJob job) {
		this.unscheduledRegistry.remove(job);
		this.scheduledRegistry.add(job);
	}

	/**
     * Moves job from scheduled to unscheduled jobs registry.
     * Only registry change, no adjustments or job data verifications
     * 
     * @param job Job to move to scheduled jobs registry 
	 */
	public void moveToUnscheduled(AbstractJob job) {
		this.scheduledRegistry.remove(job);
		this.unscheduledRegistry.add(job);
	}
}
