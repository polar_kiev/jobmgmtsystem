/**
 * 
 */
package com.jobmgmt.event;

import org.springframework.context.ApplicationEvent;

/**
 * Custom Spring event, for notification within application that job status/schedule/etc changed
 * 
 * @author Sergii Sinelnychenko
 *
 */
public class RescheduleJobsEvent extends ApplicationEvent {

	/**
	 * Required for serializable objects  
	 */
	private static final long serialVersionUID = 3449036336272147869L;

	/**
	 * Default inherited constructor
	 * @param source
	 */
	public RescheduleJobsEvent(Object source) {
		super(source);
	}

}
