package com.jobmgmt.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * Configuration for task scheduler
 * 
 * @author Sergii Sinelnychenko
 *
 */

@Configuration
@EnableScheduling
public class ApplicationSchedulingConfigurerConfiguration {

    @Autowired
    ThreadPoolTaskExecutorProperties threadPoolTaskExecutorProperties;

    @Bean
    @Qualifier("threadPoolTaskScheduler")
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler instance = new ThreadPoolTaskScheduler();
        instance.setPoolSize(threadPoolTaskExecutorProperties.getMaxPoolSize());
        return instance;
    }
}
