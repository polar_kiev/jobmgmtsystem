package com.jobmgmt.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Sergii Sinelnychenko
 *
 */
@Configuration
@ConfigurationProperties(prefix = "jobs")
@Getter
@Setter
public class JobsManagementProperties {

    private String[] packages;
}
