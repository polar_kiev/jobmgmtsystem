package com.jobmgmt.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.jobmgmt.bean.StatusBean;

/**
 * Main configuration class
 * 
 * @author Sergii Sinelnychenko
 *
 */

@Configuration
@EnableWebMvc
@EnableAsync
public class ApplicationMainConfiguration {

    @Autowired
    ThreadPoolTaskExecutorProperties threadPoolTaskExecutorProperties;

    @Bean
    public StatusBean getHealthCheckBean() {
        return new StatusBean("Everything is fine", System.currentTimeMillis());
    }

    @Bean(name = "taskExecutor", destroyMethod = "shutdown")
    @Qualifier("taskExecutor")
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor instance = new ThreadPoolTaskExecutor();
        instance.setQueueCapacity(threadPoolTaskExecutorProperties.getQueueCapacity());
        instance.setCorePoolSize(threadPoolTaskExecutorProperties.getCorePoolSize());
        instance.setMaxPoolSize(threadPoolTaskExecutorProperties.getMaxPoolSize());
        return instance;
    }
}
