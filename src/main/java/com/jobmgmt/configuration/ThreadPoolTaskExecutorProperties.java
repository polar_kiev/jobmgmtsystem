package com.jobmgmt.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Sergii Sinelnychenko
 *
 */
@Configuration
@ConfigurationProperties(prefix = "threadpool")
@Getter
@Setter
public class ThreadPoolTaskExecutorProperties {

    private int corePoolSize;

    private int maxPoolSize;

    private int queueCapacity;
}
