package com.jobmgmt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jobmgmt.bean.Schedule;
import com.jobmgmt.component.JobManagementService;
import com.jobmgmt.component.JobsRegistry;
import com.jobmgmt.job.AbstractJob;

/**
 * Controller for managing jobs schedule
 * Main path is <code>/jobs/{jobuuid}/schedule</code>, different HTTP methods available
 * 
 * @author Sergii Sinelnychenko
 */
@RestController
@RequestMapping(path = "/jobs/{jobuuid}/schedule", 
				produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public class ScheduleController {

	@Autowired
	private JobsRegistry jobsRegistry;

	@Autowired
	private JobManagementService jobManagementService;

    /**
     * GET /jobs/{jobuuid}/schedule
     * 
     * @param jobuuid UUID of the job
     * @return Schedule value for the specified job, or <code>HTTP 404</code> if no job found
     */
	@GetMapping
	public ResponseEntity<String> getJobSchedule(@PathVariable String jobuuid) {
		AbstractJob job = jobsRegistry.findJobByUuid(jobuuid);
        if (job == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(job.getScheduleString());
        }
	}

    /**
     * DELETE /jobs/{jobuuid}/schedule<br/>
     * Unschedules the job specified
     * 
     * @param jobuuid UUID of the job
     * @return <code>HTTP 200</code> for success, or <code>HTTP 400</code> with message if any error occurred
     * @see JobManagementService#unscheduleJob(String)
     */
	@DeleteMapping
    public ResponseEntity<String> deleteJobSchedule(@PathVariable String jobuuid) {
        if (!jobManagementService.unscheduleJob(jobuuid)) {
            return new ResponseEntity<String>("Unscheduling failed", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("Job unscheduled successfully", HttpStatus.OK);
    }

    /**
     * POST /jobs/{jobuuid}/schedule<br/>
     * Sets new schedule for the specified job
     * 
     * @param jobuuid UUID of the job
     * @return <ul>
     * <li><code>HTTP 200</code> for success
     * <li><code>HTTP 400</code> with message if any error occurred
     * <li><code>HTTP 406</code> if schedule data is not correct
     * @see JobManagementService#applyNewSchedule(String, Schedule)
     */
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<String> applyNewSchedule(@PathVariable String jobuuid, @RequestBody Schedule schedule) {
		if (!schedule.isScheduleValid()) {
			return new ResponseEntity<String>("Requested schedule is invalid", HttpStatus.NOT_ACCEPTABLE);
		}
		String result = jobManagementService.applyNewSchedule(jobuuid, schedule);
		if (result != null) {
			return new ResponseEntity<String>("Error: " + result, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("New schedule applied successfully", HttpStatus.OK);
	}

}
