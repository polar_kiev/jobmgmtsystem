package com.jobmgmt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jobmgmt.bean.Priority;
import com.jobmgmt.component.JobManagementService;
import com.jobmgmt.component.JobsRegistry;
import com.jobmgmt.job.AbstractJob;

/**
 * Controller for managing jobs priorities
 * Main path is <code>/jobs/{jobuuid}/priority</code>, different HTTP methods available
 * 
 * @author Sergii Sinelnychenko
 *
 */
@RestController
@RequestMapping(path = "/jobs/{jobuuid}/priority", 
				produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public class PriorityController {

	@Autowired
	private JobsRegistry jobsRegistry;

	@Autowired
	private JobManagementService jobManagementService;

	/**
	 * GET /jobs/{jobuuid}/priority
	 * 
     * @param jobuuid UUID of the job
     * @return Priority value for the specified job, or <code>HTTP 404</code> if no job found
	 */
	@GetMapping
	public ResponseEntity<Integer> getJobPriority(@PathVariable String jobuuid) {
		AbstractJob job = jobsRegistry.findJobByUuid(jobuuid);
		if (job == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(job.getPriority());
		}
	}

    /**
     * DELETE /jobs/{jobuuid}/priority
     * 
     * @param jobuuid UUID of the job
     * @return Resets (set to -1) priority value for the specified job, or <code>HTTP 400</code> with message if any error occurred
     * @see JobManagementService#applyNewPriority(String, int)
     */
	@DeleteMapping
	public ResponseEntity<String> dropJobPriority(@PathVariable String jobuuid) {
		String callResult = jobManagementService.applyNewPriority(jobuuid, -1);
        if (callResult != null) {
			return new ResponseEntity<String>("Priority dropping failed: " + callResult, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Job priority dropped successfully", HttpStatus.OK);
	}

    /**
     * POST /jobs/{jobuuid}/priority<br/>
     * Sets new priority value for the specified job
     * 
     * @param jobuuid UUID of the job
     * @return <code>HTTP 200</code> for success, <code>HTTP 400</code> with message if any error occurred
     * @see JobManagementService#applyNewPriority(String, int)
     */
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<String> applyNewSchedule(@PathVariable String jobuuid, @RequestBody Priority schedule) {
		String callResult = jobManagementService.applyNewPriority(jobuuid, schedule.getPriority());
        if (callResult != null) {
			return new ResponseEntity<String>("Error: " + callResult, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("New priority applied successfully", HttpStatus.OK);
	}
}
