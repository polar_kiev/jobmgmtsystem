package com.jobmgmt.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jobmgmt.bean.JobDetail;
import com.jobmgmt.component.JobManagementService;
import com.jobmgmt.component.JobsRegistry;
import com.jobmgmt.job.AbstractJob;

/**
 * Controller for job management requests handling
 * Main path is <code>/jobs</code>, different HTTP methods and nested URLs available
 * 
 * @author Sergii Sinelnychenko
 *
 */

@RestController
@RequestMapping(path = "/jobs", 
				method = RequestMethod.GET,
				produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class JobsRegistryController {
	
	@Autowired
	private JobsRegistry jobsRegistry;

	@Autowired
	private JobManagementService jobManagementService;

	/**
	 * GET /jobs
	 * 
	 * @return List of all jobs
	 */
	@RequestMapping(path = "")
	public @ResponseBody List<JobDetail> jobsRegistryComplete() {
		return registryToJobDetailConverter(jobsRegistry.getCompleteRegistry());
	}

	/**
     * GET /jobs/unscheduled
     * 
     * @return List of all unscheduled jobs
	 */
	@RequestMapping(path = "unscheduled")
	public @ResponseBody List<JobDetail> jobsRegistryUnscheduled() {
		return registryToJobDetailConverter(jobsRegistry.getUnscheduledRegistry());
	}
	
	/**
     * GET /jobs/scheduled
     * 
     * @return List of all scheduld jobs
	 */
	@RequestMapping(path = "scheduled")
	public @ResponseBody List<JobDetail> jobsRegistryScheduled() {
		return registryToJobDetailConverter(jobsRegistry.getScheduledRegistry());
	}
	
	/**
	 * GET /jobs/{uuid}
	 * 
	 * @param jobuuid UUID of the job
	 * 
	 * @return Details for the specified job only, or <code>HTTP 404</code> if no job found
	 */
	@RequestMapping(path = "{jobuuid}")
	public ResponseEntity<JobDetail> getSingleJob(@PathVariable String jobuuid) {
		AbstractJob job = jobsRegistry.findJobByUuid(jobuuid);
		if (job == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(new JobDetail(job));
		}
	}

	/**
	 * POST /jobs/{uuid}/reset<br/>
	 * 
	 * Resets state of the specified job
	 * 
     * @param jobuuid UUID of the job
     * 
	 * @return <code>HTTP 200</code> if everything is OK, <code>HTTP 404</code> if job was not found
	 * @see JobManagementService#resetJob(AbstractJob) 
	 */
	@PostMapping(path = "{jobuuid}/reset")
	public ResponseEntity<String> resetJob(@PathVariable String jobuuid) {
		AbstractJob job = jobsRegistry.findJobByUuid(jobuuid);
		if (job == null) {
			return ResponseEntity.notFound().build();
		} else {
			jobManagementService.resetJob(job);
			return ResponseEntity.ok("Job reset successfully");
		}
	}

	/**
	 * Converts job classes into model beans
	 * 
	 * @param registry Set of jobs to convert to beans
	 * @return List of model beans, to return as the result of REST call
	 */
	private final List<JobDetail> registryToJobDetailConverter(Set<AbstractJob> registry) {
		return registry.stream()
					   .map(JobDetail::new)
				       .collect(Collectors.toList());
	}
}
