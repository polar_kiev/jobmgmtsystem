package com.jobmgmt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jobmgmt.bean.StatusBean;

/**
 * "Walkling skeleton" for health check endpoint.
 * Implemented as a reference, no metrics added for now
 * Available by <code>/healthcheck</code> URL of the application
 * @author Sergii Sinelnychenko
 *
 */

@RestController
public class StatusCheckController {
	
	@Autowired
	private StatusBean healthCheckBean;

	@RequestMapping(path = "/healthcheck",
					method = RequestMethod.GET,
					produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public @ResponseBody StatusBean healthcheck() {
		return healthCheckBean;
	}
}
