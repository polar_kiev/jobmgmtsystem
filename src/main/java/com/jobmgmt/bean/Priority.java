/**
 * 
 */
package com.jobmgmt.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * This POJO bean is used as model bean for REST endpoints for job priority
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Priority {

	private int priority;

}
