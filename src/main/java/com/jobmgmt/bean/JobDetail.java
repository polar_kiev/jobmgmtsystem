package com.jobmgmt.bean;

import com.jobmgmt.job.AbstractJob;
import com.jobmgmt.job.JobStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This POJO bean is used as model bean for REST endpoints for job details
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class JobDetail {
	
	private String uuid;
	
	private String name;
	
	private String className;

	private int priority;
	
	private JobStatus status;
	
	private String schedule;
	
	public JobDetail(AbstractJob job) {
		this(job.getUuid(), 
			 job.getName(),
			 job.getClass().getName(), 
			 job.getPriority(), 
			 job.getStatus(), 
			 job.getScheduleString()
			);
	}

}
