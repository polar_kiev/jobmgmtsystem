/**
 * 
 */
package com.jobmgmt.bean;

import org.springframework.util.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * This POJO bean is used as model bean for REST endpoints for job schedule
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Builder
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Schedule {

	private String cron;
	private long fixedDelay;
	private String fixedDelayString;
	private long fixedRate;
	private String fixedRateString;
	private long initialDelay;
	private String initialDelayString;
	private String zone;

	/**
	 * Verifies is this instance properly set. Since settings are sometimes mutually exclusive - need 
	 * to verify that bean is initialized properly
	 */
	public boolean isScheduleValid() {
		byte counter = 0;
		if (fixedDelay > 0) {
			counter++;
		}
		if (fixedRate > 0) {
			counter++;
		}
		if (StringUtils.hasText(cron)) {
			counter++;
		}
		return counter == 1;
	}

}
