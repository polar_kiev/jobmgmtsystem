package com.jobmgmt.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This POJO bean is used as model bean for REST endpoints for health status data
 * 
 * @author Sergii Sinelnychenko
 *
 */

@AllArgsConstructor
public class StatusBean {

	@Getter
	@Setter
	private String message;

	private long startupTime;

	public long getRunningTimeSeconds() {
		return (System.currentTimeMillis() - this.startupTime) / 1000;
	}
}
