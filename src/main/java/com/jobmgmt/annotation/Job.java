package com.jobmgmt.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * This annotation should be used on descendants of {@link com.jobmgmt.job.AbstractJob AbstractJob} to indicate Job component.
 * Allows initial configuration of job's schedule or priority
 * 
 * @author Sergii Sinelnychenko
 *
 */
@Component
@Retention(RUNTIME)
@Target(TYPE)
public @interface Job {

	public int priority() default -1;

	public int fixedDelay() default -1;

	public int fixedRate() default -1;

	public String cron() default "";

	public String zone() default "";
}
