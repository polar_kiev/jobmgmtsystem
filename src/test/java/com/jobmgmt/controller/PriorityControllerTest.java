package com.jobmgmt.controller;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.jobmgmt.Constants;
import com.jobmgmt.component.JobManagementService;
import com.jobmgmt.component.JobsRegistry;
import com.jobmgmt.job.AbstractJob;

/**
 * @author Sergii Sinelnychenko
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PriorityControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    JobsRegistry jobRegistry;

    @MockBean
    JobManagementService jobManagementService;

    @Mock
    AbstractJob abstractJob1;
    @Mock
    AbstractJob abstractJob2;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(abstractJob1.getName()).thenReturn(Constants.NAME_JOB1);
        when(abstractJob1.getPriority()).thenReturn(Constants.PRIORITY_JOB1);
        when(abstractJob1.getScheduleString()).thenReturn(Constants.SCHEDULE_JOB1);
        when(abstractJob1.getStatus()).thenReturn(Constants.STATUS_JOB1);
        when(abstractJob1.getUuid()).thenReturn(Constants.UUID_JOB1);

        when(abstractJob2.getName()).thenReturn(Constants.NAME_JOB2);
        when(abstractJob2.getPriority()).thenReturn(Constants.PRIORITY_JOB2);
        when(abstractJob2.getScheduleString()).thenReturn(Constants.SCHEDULE_JOB2);
        when(abstractJob2.getStatus()).thenReturn(Constants.STATUS_JOB2);
        when(abstractJob2.getUuid()).thenReturn(Constants.UUID_JOB2);
    }

    @Test
    public void verifySuccessfulGet() throws Exception {
        when(jobRegistry.findJobByUuid(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1 + "/priority")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().string(String.valueOf(Constants.PRIORITY_JOB1)));
        verify(jobRegistry).findJobByUuid(eq(Constants.UUID_JOB1));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifySearchByIdNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1 + "/priority")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        verify(jobRegistry).findJobByUuid(eq(Constants.UUID_JOB1));
        verifyNoMoreInteractions(jobRegistry);
        verifyNoMoreInteractions(jobManagementService);
    }

    @Test
    public void verifySuccessfulDelete() throws Exception {
        when(jobManagementService.applyNewPriority(anyString(), anyInt())).thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.delete("/jobs/" + Constants.UUID_JOB1 + "/priority")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().string("Job priority dropped successfully"));
        verify(jobManagementService).applyNewPriority(eq(Constants.UUID_JOB1), eq(-1));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifyFailedlDelete() throws Exception {
        when(jobManagementService.applyNewPriority(anyString(), anyInt())).thenReturn("Job not found");
        mvc.perform(MockMvcRequestBuilders.delete("/jobs/" + Constants.UUID_JOB1 + "/priority")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(content().string("Priority dropping failed: Job not found"));
        verify(jobManagementService).applyNewPriority(eq(Constants.UUID_JOB1), eq(-1));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifySuccessfulPost() throws Exception {
        when(jobManagementService.applyNewPriority(anyString(), anyInt())).thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.post("/jobs/" + Constants.UUID_JOB1 + "/priority")
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                .content("{\"priority\" : 100}")).andExpect(status().isOk())
                .andExpect(content().string("New priority applied successfully"));
        verify(jobManagementService).applyNewPriority(eq(Constants.UUID_JOB1), eq(100));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifyFailedPost() throws Exception {
        when(jobManagementService.applyNewPriority(anyString(), anyInt())).thenReturn("Job not found");
        mvc.perform(MockMvcRequestBuilders.post("/jobs/" + Constants.UUID_JOB1 + "/priority")
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                .content("{\"priority\" : 100}")).andExpect(status().isBadRequest())
                .andExpect(content().string("Error: Job not found"));
        verify(jobManagementService).applyNewPriority(eq(Constants.UUID_JOB1), eq(100));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

}
