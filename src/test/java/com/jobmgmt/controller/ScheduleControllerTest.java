package com.jobmgmt.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.jobmgmt.Constants;
import com.jobmgmt.bean.Schedule;
import com.jobmgmt.component.JobManagementService;
import com.jobmgmt.component.JobsRegistry;
import com.jobmgmt.job.AbstractJob;

/**
 * @author Sergii Sinelnychenko
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ScheduleControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    JobsRegistry jobRegistry;

    @MockBean
    JobManagementService jobManagementService;

    @Mock
    AbstractJob abstractJob1;
    @Mock
    AbstractJob abstractJob2;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(abstractJob1.getName()).thenReturn(Constants.NAME_JOB1);
        when(abstractJob1.getPriority()).thenReturn(Constants.PRIORITY_JOB1);
        when(abstractJob1.getScheduleString()).thenReturn(Constants.SCHEDULE_JOB1);
        when(abstractJob1.getStatus()).thenReturn(Constants.STATUS_JOB1);
        when(abstractJob1.getUuid()).thenReturn(Constants.UUID_JOB1);

        when(abstractJob2.getName()).thenReturn(Constants.NAME_JOB2);
        when(abstractJob2.getPriority()).thenReturn(Constants.PRIORITY_JOB2);
        when(abstractJob2.getScheduleString()).thenReturn(Constants.SCHEDULE_JOB2);
        when(abstractJob2.getStatus()).thenReturn(Constants.STATUS_JOB2);
        when(abstractJob2.getUuid()).thenReturn(Constants.UUID_JOB2);
    }

    @Test
    public void verifySuccessfulGet() throws Exception {
        when(jobRegistry.findJobByUuid(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1 + "/schedule")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().string(String.valueOf(Constants.SCHEDULE_JOB1)));
        verify(jobRegistry).findJobByUuid(eq(Constants.UUID_JOB1));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifySearchByIdNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1 + "/schedule")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        verify(jobRegistry).findJobByUuid(eq(Constants.UUID_JOB1));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifySuccessfulDelete() throws Exception {
        when(jobManagementService.unscheduleJob(anyString())).thenReturn(true);
        mvc.perform(MockMvcRequestBuilders.delete("/jobs/" + Constants.UUID_JOB1 + "/schedule")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().string("Job unscheduled successfully"));
        verify(jobManagementService).unscheduleJob(eq(Constants.UUID_JOB1));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifyFailedlDelete() throws Exception {
        when(jobManagementService.unscheduleJob(anyString())).thenReturn(false);
        mvc.perform(MockMvcRequestBuilders.delete("/jobs/" + Constants.UUID_JOB1 + "/schedule")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(content().string("Unscheduling failed"));
        verify(jobManagementService).unscheduleJob(eq(Constants.UUID_JOB1));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifySuccessfulPost() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/jobs/" + Constants.UUID_JOB1 + "/schedule")
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                .content("{\"fixedDelay\":3000}")).andExpect(status().isOk())
                .andExpect(content().string("New schedule applied successfully"));
        verify(jobManagementService).applyNewSchedule(eq(Constants.UUID_JOB1), any(Schedule.class));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifyFailedPost() throws Exception {
        when(jobManagementService.applyNewSchedule(anyString(), any(Schedule.class))).thenReturn("Error from test");
        mvc.perform(MockMvcRequestBuilders.post("/jobs/" + Constants.UUID_JOB1 + "/schedule")
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                .content("{\"fixedDelay\":3000}")).andExpect(status().isBadRequest())
                .andExpect(content().string("Error: Error from test"));
        verify(jobManagementService).applyNewSchedule(eq(Constants.UUID_JOB1), any(Schedule.class));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifyFailedPostWithWrongSchedule() throws Exception {
        when(jobManagementService.applyNewSchedule(anyString(), any(Schedule.class))).thenReturn("Error from test");
        mvc.perform(MockMvcRequestBuilders.post("/jobs/" + Constants.UUID_JOB1 + "/schedule")
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                .content("{\"fixedDelay\":3000, \"cron\":\"0 0 0 0 0 1 1\"}")).andExpect(status().isNotAcceptable())
                .andExpect(content().string("Requested schedule is invalid"));
        verifyNoMoreInteractions(jobManagementService);
        verifyNoMoreInteractions(jobRegistry);
    }

}
