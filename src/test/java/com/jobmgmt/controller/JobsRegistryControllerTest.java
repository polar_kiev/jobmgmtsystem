package com.jobmgmt.controller;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.jobmgmt.Constants;
import com.jobmgmt.component.JobManagementService;
import com.jobmgmt.component.JobsRegistry;
import com.jobmgmt.job.AbstractJob;

/**
 * @author Sergii Sinelnychenko
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JobsRegistryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    JobsRegistry jobRegistry;

    @MockBean
    JobManagementService jobManagementService;

    @Mock
    AbstractJob abstractJob1;
    @Mock
    AbstractJob abstractJob2;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(abstractJob1.getName()).thenReturn(Constants.NAME_JOB1);
        when(abstractJob1.getPriority()).thenReturn(Constants.PRIORITY_JOB1);
        when(abstractJob1.getScheduleString()).thenReturn(Constants.SCHEDULE_JOB1);
        when(abstractJob1.getStatus()).thenReturn(Constants.STATUS_JOB1);
        when(abstractJob1.getUuid()).thenReturn(Constants.UUID_JOB1);

        when(abstractJob2.getName()).thenReturn(Constants.NAME_JOB2);
        when(abstractJob2.getPriority()).thenReturn(Constants.PRIORITY_JOB2);
        when(abstractJob2.getScheduleString()).thenReturn(Constants.SCHEDULE_JOB2);
        when(abstractJob2.getStatus()).thenReturn(Constants.STATUS_JOB2);
        when(abstractJob2.getUuid()).thenReturn(Constants.UUID_JOB2);

    }

    @Test
    public void testProperJobToJobDetailsConvertion() throws Exception {
        when(jobRegistry.getCompleteRegistry()).thenReturn(Sets.newSet(abstractJob1));

        mvc.perform(MockMvcRequestBuilders.get("/jobs").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", equalTo(Constants.NAME_JOB1)))
                .andExpect(jsonPath("$[0].priority", equalTo(Constants.PRIORITY_JOB1)))
                .andExpect(jsonPath("$[0].schedule", equalTo(Constants.SCHEDULE_JOB1)))
                .andExpect(jsonPath("$[0].status", equalTo(Constants.STATUS_JOB1.toString())))
                .andExpect(jsonPath("$[0].uuid", equalTo(Constants.UUID_JOB1)));

        verify(jobRegistry).getCompleteRegistry();
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void testProperMultipleJobsToJobDetailsConvertion() throws Exception {
        when(jobRegistry.getCompleteRegistry()).thenReturn(Sets.newSet(abstractJob1, abstractJob2));

        mvc.perform(MockMvcRequestBuilders.get("/jobs").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].['name']", hasItems(Constants.NAME_JOB1, Constants.NAME_JOB2)))
                .andExpect(jsonPath("$[*].['priority']", hasItems(Constants.PRIORITY_JOB1, Constants.PRIORITY_JOB2)))
                .andExpect(jsonPath("$[*].['schedule']", hasItems(Constants.SCHEDULE_JOB1, Constants.SCHEDULE_JOB2)))
                .andExpect(jsonPath("$[*].['status']",
                        hasItems(Constants.STATUS_JOB1.toString(), Constants.STATUS_JOB2.toString())))
                .andExpect(jsonPath("$[*].['uuid']", hasItems(Constants.UUID_JOB2, Constants.UUID_JOB1)));

        verify(jobRegistry).getCompleteRegistry();
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifyNoMixtureScheduledWithUnscheduledJobs() throws Exception {
        when(jobRegistry.getScheduledRegistry()).thenReturn(Sets.newSet(abstractJob1));
        when(jobRegistry.getUnscheduledRegistry()).thenReturn(Sets.newSet(abstractJob2));

        mvc.perform(MockMvcRequestBuilders.get("/jobs/scheduled").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[*].['name']", contains(Constants.NAME_JOB1)))
                .andExpect(jsonPath("$[*].['priority']", contains(Constants.PRIORITY_JOB1)))
                .andExpect(jsonPath("$[*].['schedule']", contains(Constants.SCHEDULE_JOB1)))
                .andExpect(jsonPath("$[*].['status']", contains(Constants.STATUS_JOB1.toString())))
                .andExpect(jsonPath("$[*].['uuid']", contains(Constants.UUID_JOB1)));

        verify(jobRegistry).getScheduledRegistry();
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifyNoMixtureUnScheduledWithScheduledJobs() throws Exception {
        when(jobRegistry.getScheduledRegistry()).thenReturn(Sets.newSet(abstractJob1));
        when(jobRegistry.getUnscheduledRegistry()).thenReturn(Sets.newSet(abstractJob2));

        mvc.perform(MockMvcRequestBuilders.get("/jobs/unscheduled").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[*].['name']", contains(Constants.NAME_JOB2)))
                .andExpect(jsonPath("$[*].['priority']", contains(Constants.PRIORITY_JOB2)))
                .andExpect(jsonPath("$[*].['schedule']", contains(Constants.SCHEDULE_JOB2)))
                .andExpect(jsonPath("$[*].['status']", contains(Constants.STATUS_JOB2.toString())))
                .andExpect(jsonPath("$[*].['uuid']", contains(Constants.UUID_JOB2)));

        verify(jobRegistry).getUnscheduledRegistry();
        verifyNoMoreInteractions(jobRegistry);
    }

    @Test
    public void verifySearchByIdSuccessful() throws Exception {
        when(jobRegistry.findJobByUuid(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.['name']", is(Constants.NAME_JOB1)))
                .andExpect(jsonPath("$.['priority']", is(Constants.PRIORITY_JOB1)))
                .andExpect(jsonPath("$.['schedule']", is(Constants.SCHEDULE_JOB1)))
                .andExpect(jsonPath("$.['status']", is(Constants.STATUS_JOB1.toString())))
                .andExpect(jsonPath("$.['uuid']", is(Constants.UUID_JOB1)));
    }

    @Test
    public void verifySearchByIdNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void verifyResetSuccessful() throws Exception {
        when(jobRegistry.findJobByUuid(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1 + "/reset")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().string("Job reset successfully"));
        verify(jobManagementService).resetJob(eq(abstractJob1));
        verifyNoMoreInteractions(jobManagementService);
    }

    @Test
    public void verifyResetNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/jobs/" + Constants.UUID_JOB1 + "/reset")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }
}
