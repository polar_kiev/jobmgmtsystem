package com.jobmgmt.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.jobmgmt.bean.StatusBean;

/**
 * @author Sergii Sinelnychenko
 *
 */

@TestConfiguration
@EnableConfigurationProperties
@ConfigurationProperties
@Import(value = { ThreadPoolTaskExecutorProperties.class })
public class ApplicationTestConfiguration {

    @Autowired
    ThreadPoolTaskExecutorProperties threadPoolTaskExecutorProperties;

    @Bean
    public StatusBean getHealthCheckBean() {
        return new StatusBean("Everything is fine", System.currentTimeMillis());
    }

    @Bean(name = "taskExecutor", destroyMethod = "shutdown")
    @Qualifier("taskExecutor")
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor instance = new ThreadPoolTaskExecutor();
        instance.setQueueCapacity(threadPoolTaskExecutorProperties.getQueueCapacity());
        instance.setCorePoolSize(threadPoolTaskExecutorProperties.getCorePoolSize());
        instance.setMaxPoolSize(threadPoolTaskExecutorProperties.getMaxPoolSize());
        return instance;
    }
}
