package com.jobmgmt;

import com.jobmgmt.job.JobStatus;

/**
 * Constants to be used in tests
 * 
 * @author Sergii Sinelnychenko
 */
public class Constants {

    public static final JobStatus STATUS_JOB2 = JobStatus.RUNNING;

    public static final JobStatus STATUS_JOB1 = JobStatus.QUEUED;

    public static final int PRIORITY_JOB2 = 20;

    public static final int PRIORITY_JOB1 = 10;

    public static final String SCHEDULE_JOB2 = "Schedule(cron=0/30 * * * * *, fixedDelay=-1, fixedDelayString=null, fixedRate=-1, fixedRateString=null, initialDelay=0, initialDelayString=null, zone=)";

    public static final String SCHEDULE_JOB1 = "Not scheduled";

    public static final String NAME_JOB2 = "TestJob2";

    public static final String NAME_JOB1 = "TestJob1";

    public static final String UUID_JOB2 = "699f9b70-5555-44f6-8e3b-b1382fec933b";

    public static final String UUID_JOB1 = "699f9b70-4469-44f6-8e3b-b1382fec933b";

}
