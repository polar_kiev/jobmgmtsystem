package com.jobmgmt.component.samplejobs.scheduled;

import com.jobmgmt.annotation.Job;
import com.jobmgmt.job.AbstractJob;

/**
 * @author Sergii Sinelnychenko
 *
 */
@Job(cron = "0/30 * * * * *")
public class ScheduledPrintJobCronTest extends AbstractJob {

    public static final String FAKE_UUID = "Scheduled fake UUID for tests";

    public ScheduledPrintJobCronTest() {
        super();
    }

    @Override
    public boolean doRun() {
        log.info("Cron Print job here!");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.warn("Interrupted", e);
            return false;
        }
        return true;
    }

    @Override
    public String getUuid() {
        return FAKE_UUID;
    }

}
