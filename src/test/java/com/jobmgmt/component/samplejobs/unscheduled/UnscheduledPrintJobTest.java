package com.jobmgmt.component.samplejobs.unscheduled;

import com.jobmgmt.annotation.Job;
import com.jobmgmt.job.AbstractJob;

/**
 * @author Sergii Sinelnychenko
 *
 */
@Job(priority = 100)
public class UnscheduledPrintJobTest extends AbstractJob {

    public static final String FAKE_UUID = "Unscheduled fake UUID for tests";

    public UnscheduledPrintJobTest() {
        super();
    }

    @Override
    public boolean doRun() {
        log.info("Prioritized Print job here!");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.warn("Interrupted", e);
            return false;
        }
        return true;
    }

    @Override
    public String getUuid() {
        return FAKE_UUID;
    }

}
