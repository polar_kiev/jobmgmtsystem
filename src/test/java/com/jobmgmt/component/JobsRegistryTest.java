package com.jobmgmt.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;

import com.jobmgmt.Constants;
import com.jobmgmt.component.samplejobs.scheduled.ScheduledPrintJobCronTest;
import com.jobmgmt.component.samplejobs.unscheduled.UnscheduledPrintJobTest;
import com.jobmgmt.configuration.ApplicationTestConfiguration;
import com.jobmgmt.configuration.JobsManagementProperties;
import com.jobmgmt.job.AbstractJob;

/**
 * @author Sergii Sinelnychenko
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ApplicationTestConfiguration.class, JobsRegistry.class })
@TestExecutionListeners(MockitoTestExecutionListener.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class JobsRegistryTest extends AbstractJUnit4SpringContextTests {

    @MockBean
    JobManagementService jobManagementService;

    @MockBean
    JobsManagementProperties jobsManagementProperties;

    @Mock
    AbstractJob abstractJob1;
    @Mock
    AbstractJob abstractJob2;

    @Autowired
    private JobsRegistry jobRegistry;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testEmptyRegistry() throws Exception {

        assertNull("There should be no jobs in registry", jobRegistry.findJobByUuid(Constants.UUID_JOB1));
        // This happens due to @PostConstruct
        verify(jobsManagementProperties).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }

    @Test
    public void testOneJobInRegistry() throws Exception {
        when(jobsManagementProperties.getPackages()).thenReturn(new String[] { "com.jobmgmt.component.samplejobs" });
        jobRegistry.scan();

        // no jobs because job class is not registered with spring context
        assertNotNull("There should be the registry", jobRegistry.getCompleteRegistry());
        assertEquals("There should be one jobs in registry", 2, jobRegistry.getCompleteRegistry().size());
        // This happens due to @PostConstruct
        verify(jobsManagementProperties, times(2)).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }

    @Test
    public void testOneScheduledJobInRegistry() throws Exception {
        when(jobsManagementProperties.getPackages()).thenReturn(new String[] { "com.jobmgmt.component.samplejobs" });
        jobRegistry.scan();

        // no jobs because job class is not registered with spring context
        assertNotNull("There should be the registry", jobRegistry.getCompleteRegistry());
        assertEquals("There should be one scheduled jobs in registry", 1, jobRegistry.getScheduledRegistry().size());
        // This happens due to @PostConstruct
        verify(jobsManagementProperties, times(2)).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }

    @Test
    public void testOneUncheduledJobInRegistry() throws Exception {
        when(jobsManagementProperties.getPackages()).thenReturn(new String[] { "com.jobmgmt.component.samplejobs" });
        jobRegistry.scan();

        // no jobs because job class is not registered with spring context
        assertNotNull("There should be the registry", jobRegistry.getCompleteRegistry());
        assertEquals("There should be one unscheduled jobs in registry", 1,
                jobRegistry.getUnscheduledRegistry().size());
        // This happens due to @PostConstruct
        verify(jobsManagementProperties, times(2)).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }

    @Test
    public void testSearchScheduledByUUID() throws Exception {
        when(jobsManagementProperties.getPackages()).thenReturn(new String[] { "com.jobmgmt.component.samplejobs" });
        jobRegistry.scan();

        assertNotNull("There should be the job", jobRegistry.findJobByUuid(ScheduledPrintJobCronTest.FAKE_UUID));
        assertNotNull("There should be the job", jobRegistry.findScheduledJob(ScheduledPrintJobCronTest.FAKE_UUID));
        // This happens due to @PostConstruct
        verify(jobsManagementProperties, times(2)).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }

    @Test
    public void testSearchUncheduledByUUID() throws Exception {
        when(jobsManagementProperties.getPackages()).thenReturn(new String[] { "com.jobmgmt.component.samplejobs" });
        jobRegistry.scan();

        assertNotNull("There should be the job", jobRegistry.findJobByUuid(UnscheduledPrintJobTest.FAKE_UUID));
        assertNotNull("There should be the job", jobRegistry.findUnscheduledJob(UnscheduledPrintJobTest.FAKE_UUID));
        // This happens due to @PostConstruct
        verify(jobsManagementProperties, times(2)).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }

    @Test
    public void testMoveToScheduled() throws Exception {
        when(jobsManagementProperties.getPackages()).thenReturn(new String[] { "com.jobmgmt.component.samplejobs" });
        jobRegistry.scan();

        AbstractJob job = jobRegistry.findUnscheduledJob(UnscheduledPrintJobTest.FAKE_UUID);
        assertNotNull("There should be the job", job);
        jobRegistry.moveToScheduled(job);
        assertEquals("There should be no unscheduled jobs", 0, jobRegistry.getUnscheduledRegistry().size());
        assertEquals("All jobs must be scheduled", 2, jobRegistry.getScheduledRegistry().size());
        // This happens due to @PostConstruct
        verify(jobsManagementProperties, times(2)).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }

    @Test
    public void testMoveToUncheduled() throws Exception {
        when(jobsManagementProperties.getPackages()).thenReturn(new String[] { "com.jobmgmt.component.samplejobs" });
        jobRegistry.scan();

        AbstractJob job = jobRegistry.findScheduledJob(ScheduledPrintJobCronTest.FAKE_UUID);
        assertNotNull("There should be the job", job);
        jobRegistry.moveToUnscheduled(job);
        assertEquals("There should be no scheduled jobs", 0, jobRegistry.getScheduledRegistry().size());
        assertEquals("All jobs must be unscheduled", 2, jobRegistry.getUnscheduledRegistry().size());
        // This happens due to @PostConstruct
        verify(jobsManagementProperties, times(2)).getPackages();
        verifyNoMoreInteractions(jobsManagementProperties);
    }
}
