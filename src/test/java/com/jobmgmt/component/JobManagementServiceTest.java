package com.jobmgmt.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.concurrent.ScheduledFuture;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;

import com.jobmgmt.Constants;
import com.jobmgmt.bean.Schedule;
import com.jobmgmt.configuration.ApplicationSchedulingConfigurerConfiguration;
import com.jobmgmt.configuration.ApplicationTestConfiguration;
import com.jobmgmt.configuration.JobsManagementProperties;
import com.jobmgmt.job.AbstractJob;

/**
 * @author Sergii Sinelnychenko
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ApplicationTestConfiguration.class, JobManagementService.class,
        ApplicationSchedulingConfigurerConfiguration.class })
@TestExecutionListeners(MockitoTestExecutionListener.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class JobManagementServiceTest extends AbstractJUnit4SpringContextTests {

    @MockBean
    JobsRegistry jobsRegistry;

    @MockBean
    JobsManagementProperties jobsManagementProperties;

    @Mock
    AbstractJob abstractJob1;

    @SuppressWarnings("rawtypes")
    @Mock
    ScheduledFuture future;

    @Mock
    Schedule schedule;

    @Autowired
    private JobManagementService jobManagementService;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(abstractJob1.getName()).thenReturn(Constants.NAME_JOB1);
        when(abstractJob1.getPriority()).thenReturn(Constants.PRIORITY_JOB1);
        when(abstractJob1.getScheduleString()).thenReturn(Constants.SCHEDULE_JOB1);
        when(abstractJob1.getStatus()).thenReturn(Constants.STATUS_JOB1);
        when(abstractJob1.getUuid()).thenReturn(Constants.UUID_JOB1);
    }

    @Test
    public void testUnscheduleNonExistingJob() throws Exception {

        when(jobsRegistry.findJobByUuid(Constants.UUID_JOB1)).thenReturn(abstractJob1);

        assertFalse("Unscheduling of non existing job return must be FALSE",
                jobManagementService.unscheduleJob(Constants.UUID_JOB2));
    }

    @Test
    public void testUnscheduleJobWithNoScheduledFuture() throws Exception {

        when(jobsRegistry.findJobByUuid(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        when(abstractJob1.getFuture()).thenReturn(null);

        assertFalse("With no ScheduledFuture unscheduling result must be FALSE",
                jobManagementService.unscheduleJob(Constants.UUID_JOB1));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testUnscheduleJobWithExistingScheduledFuture() throws Exception {

        when(jobsRegistry.findJobByUuid(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        when(future.cancel(false)).thenReturn(true);
        when(abstractJob1.getFuture()).thenReturn(future);

        assertTrue("Must return same value as future.cancel(...)",
                jobManagementService.unscheduleJob(Constants.UUID_JOB1));

        verify(future).cancel(false);
    }

    @Test
    public void testResetJob() throws Exception {

        jobManagementService.resetJob(abstractJob1);

        verify(abstractJob1).reset();
        // ApplicationEventPublisher cannot be mocked due to the bug::
        // https://github.com/spring-projects/spring-framework/issues/18907
        verifyNoMoreInteractions(abstractJob1);
    }

    @Test
    public void testApplyNewScheduleForMissedJob() throws Exception {
        String result = jobManagementService.applyNewSchedule(Constants.UUID_JOB1, any(Schedule.class));
        assertEquals("Job not found", result);
    }

    @Test
    public void testApplyNewScheduleForScheduledJob() throws Exception {
        when(jobsRegistry.findScheduledJob(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        // to avoid creating of new threads
        when(abstractJob1.isScheduleValid()).thenReturn(false);

        jobManagementService.applyNewSchedule(Constants.UUID_JOB1, schedule);

        verify(abstractJob1).applyNewSchedule(schedule);

        // Trying unschedule current task
        verify(abstractJob1).getFuture();

        // Informing that failed to create new scheduled task for the job
        verify(abstractJob1).isScheduleValid();
        verify(abstractJob1).getUuid();
        verify(abstractJob1).getScheduleString();

        verify(jobsRegistry).findScheduledJob(Constants.UUID_JOB1);
        verifyNoMoreInteractions(abstractJob1);

        verify(jobsRegistry).getUnscheduledRegistry();
        verify(jobsRegistry).getScheduledRegistry();
        verifyNoMoreInteractions(jobsRegistry);
    }

    @Test
    public void testApplyNewScheduleForUncheduledJob() throws Exception {
        when(jobsRegistry.findUnscheduledJob(Constants.UUID_JOB1)).thenReturn(abstractJob1);
        // to avoid creating of new threads
        when(abstractJob1.isScheduleValid()).thenReturn(false);

        jobManagementService.applyNewSchedule(Constants.UUID_JOB1, schedule);

        verify(abstractJob1).applyNewSchedule(schedule);

        // Informing that failed to create new scheduled task for the job
        verify(abstractJob1).isScheduleValid();
        verify(abstractJob1).getUuid();
        verify(abstractJob1).getScheduleString();

        verify(jobsRegistry).findScheduledJob(Constants.UUID_JOB1);
        verify(jobsRegistry).findUnscheduledJob(Constants.UUID_JOB1);
        verify(jobsRegistry).moveToScheduled(abstractJob1);
        verifyNoMoreInteractions(abstractJob1);

        verify(jobsRegistry).getUnscheduledRegistry();
        verify(jobsRegistry).getScheduledRegistry();
        verifyNoMoreInteractions(jobsRegistry);
    }

    @Test
    public void testApplyNewPriorityForMissedJob() throws Exception {
        assertNotNull("Job should not be found", jobManagementService.applyNewPriority(Constants.UUID_JOB1, anyInt()));
    }

    @Test
    public void testApplyNewPriorityForScheduledJob() throws Exception {
        when(jobsRegistry.findScheduledJob(Constants.UUID_JOB1)).thenReturn(abstractJob1);

        jobManagementService.applyNewPriority(Constants.UUID_JOB1, Constants.PRIORITY_JOB1);

        verify(abstractJob1).setPriority(Constants.PRIORITY_JOB1);

        // Trying unschedule current task
        verify(abstractJob1).getFuture();

        verify(jobsRegistry).findScheduledJob(Constants.UUID_JOB1);
        verifyNoMoreInteractions(abstractJob1);

        verify(jobsRegistry, times(2)).getUnscheduledRegistry();
        verify(jobsRegistry).getScheduledRegistry();
        verify(jobsRegistry).moveToUnscheduled(abstractJob1);
        verifyNoMoreInteractions(jobsRegistry);
    }

    @Test
    public void testApplyNewPriorityForUnscheduledJob() throws Exception {
        when(jobsRegistry.findUnscheduledJob(Constants.UUID_JOB1)).thenReturn(abstractJob1);

        jobManagementService.applyNewPriority(Constants.UUID_JOB1, Constants.PRIORITY_JOB1);

        verify(abstractJob1).setPriority(Constants.PRIORITY_JOB1);

        verify(jobsRegistry).findScheduledJob(Constants.UUID_JOB1);
        verify(jobsRegistry).findUnscheduledJob(Constants.UUID_JOB1);
        verifyNoMoreInteractions(abstractJob1);

        verify(jobsRegistry, times(2)).getUnscheduledRegistry();
        verify(jobsRegistry).getScheduledRegistry();
        verifyNoMoreInteractions(jobsRegistry);
    }
}
